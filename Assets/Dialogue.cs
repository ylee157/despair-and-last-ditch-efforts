using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour {
    private int selection;
    public Text dialogue;
	public Text npcName;
	public List<Text> choices;
	public GameObject rightarrow;
	public PlayerInteractions player;
	public Image speaker;
	private int startingIndex;
	private DialoguePoint npc;
	private Dictionary<string, Sprite> portraits;
    Color lighter = new Color(0.9f, 0.9f, 0.9f);
    Color black = new Color(0.0f, 0.0f, 0.0f);

	void Start () {
		portraits = new Dictionary<string, Sprite>();
		portraits["Sassandra"] = Resources.Load<Sprite>("Art/portraits/sass");
		portraits[""] = Resources.Load<Sprite>("Art/pause_screen/item_icons/blank");
        portraits["Unknown"] = Resources.Load<Sprite>("Art/portraits/main");
	}

    void OnEnable () {
        rightarrow.SetActive(false);
		Camera.canPause = false;
		startingIndex = 0;
		selection = 0;
    }

	public void talkToNPC(DialoguePoint n) {
		npc = n;
		startSelection();
	}

	private void setDialogue(string words) {
		int index = words.IndexOf(":");
		string name = "";
		if(index != -1) {
			name = words.Substring(0, index);
			words = words.Substring(index + 2, words.Length - index - 2);
		}
        if(name == "SA" || name == "OC")
            name = "Sassandra";
		npcName.text = name;
        dialogue.text = words;
        if(portraits.ContainsKey(name))
            speaker.sprite = portraits[name];
        else
            speaker.sprite = portraits["Unknown"];
	}

	private void startSelection() {
		foreach(Text choice in choices)
			choice.gameObject.SetActive(npc.heavyDialogue);

		setDialogue(npc.words);

		if(!npc.heavyDialogue)
			return;
		for (int i = 0; i < choices.Count; i++)
        {
            choices[i].text = npc.options[i + startingIndex];
            choices[i].enabled = true;
        }
	}

	void Update () {
		if(!npc.heavyDialogue)
			return;


        if (Input.GetKeyDown(KeyCode.DownArrow))
			selection = (selection + 2) % 3;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            selection = (selection + 1) % 3;
		for(int i = 0; i < 2; ++i)
			if(choices[i].text == "[Press enter to continue.]")
				selection = i;

		foreach(Text t in choices)
			t.color = black;
		choices[selection].color = lighter;

        if (Input.GetButtonDown("Submit"))
        {
            npc.words = npc.responses[startingIndex + selection];
            startingIndex = npc.indices[startingIndex + selection];
            DialogueTriggers.decisionImpact(npc.words);
            if (npc.dialogueEnders.Contains(startingIndex))
            {
				setDialogue(npc.words);
                endDialogue();
            }
            else
            {
                startSelection();
            }
        }
    }


    private void endDialogue()
    {
		foreach (Text choice in choices)
            choice.enabled = false;
        npc.heavyDialogue = false;
		player.frozen = false;
        Camera.canPause = true;
    }
}
