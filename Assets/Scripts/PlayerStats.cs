﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using System.IO;

public class PlayerStats
{

    //change everything to a single dictionary<string, <dictionary<string, int>> tbh
    public static SortedDictionary<string, int> items;
    public static int Experience;

    public static List<string> partyMembers;
    public static Dictionary<string, int> maxHealth;
    public static Dictionary<string, int> currentHealth;
    public static Dictionary<string, int> maxSpirit;
    public static int upgradesAvailable;
    public static Dictionary<string, int> upgrades;
    public static Dictionary<string, string> weaponsEquipped;
    public static Vector3 playerPos;
    public static Vector3 followerPos;

    public static Dictionary<string, int> MainStats;
    public static Dictionary<string, int> SassandraStats;
    public static Dictionary<string, int> LarsStats;
    public static Dictionary<string, int> UgudStats;
    public static Dictionary<string, List<string>> PlayerAttacks;
    public static Dictionary<string, List<string>> PlayerSpells;
    public static Dictionary<string, string> SkillDescriptions;

    public static Dictionary<string, Move> Moves;

    static PlayerStats()
    {
        Experience = 99;
        items = new SortedDictionary<string, int>();
        items["Jellyfish Jelly"] = 5;
        items["Vapor"] = 7;
        items["Dead Graveworm"] = 11;
        items["Hue's Paintball Gun"] = 1;
        items["Skillixir"] = 8;
        items["Garbage"] = 2;
        items["Red Lute"] = 1;
        items["Black Lute"] = 1;
        items["White Lute"] = 1;
        items["healthpack"] = 3;
        for (int i = 0; i < 20; ++i)
        {
            items["z_PH_ITEM" + i.ToString()] = 2;
        }


        partyMembers = new List<string> { "MAIN", "Sassandra", "Lars", "Ugud" };

        maxHealth = new Dictionary<string, int>();
        maxHealth["MAIN"] = 100;
        maxHealth["Sassandra"] = 60;
        maxHealth["Lars"] = 100;
        maxHealth["Ugud"] = 140;

        currentHealth = new Dictionary<string, int>();
        foreach (string name in partyMembers)
        {
            currentHealth[name] = maxHealth[name] - 1;
        }

        maxSpirit = new Dictionary<string, int>();

        maxSpirit["MAIN"] = 3;
        maxSpirit["Sassandra"] = 2;
        maxSpirit["Lars"] = 2;
        maxSpirit["Ugud"] = 2;

        /*
        attackDamage = new Dictionary<string, int>();
        defense = new Dictionary<string, int>();
        */
        followerPos = new Vector3(-144, 6, 0);
        playerPos = new Vector3(-141, 6, 0);

        upgradesAvailable = 0;

        upgrades = new Dictionary<string, int>();

        upgrades["Fire"] = 0;
        upgrades["Ice"] = 0;
        upgrades["Defense"] = 0;
        upgrades["Melee"] = 0;
        upgrades["Ranged"] = 0;
        upgrades["Buff"] = 0;

        weaponsEquipped = new Dictionary<string, string>();
        weaponsEquipped["MAIN"] = "----------";
        weaponsEquipped["Sassandra"] = "----------";
        weaponsEquipped["Lars"] = "wooden lute";
        weaponsEquipped["Ugud"] = "----------";

        MainStats = new Dictionary<string, int>();
        MainStats["MaxHP"] = 80;
        MainStats["CurrentHP"] = 50;
        MainStats["MaxSpirit"] = 2;
        MainStats["Melee"] = 10;
        MainStats["Ranged"] = 10;
        MainStats["Fire"] = 0; // Makes them take more damage
        MainStats["Ice"] = 0; // Makes them deal less damage
        MainStats["Buff"] = 0;
        MainStats["Defense"] = 10;


        SassandraStats = new Dictionary<string, int>();
        SassandraStats["MaxHP"] = 60;
        SassandraStats["CurrentHP"] = 40;
        SassandraStats["MaxSpirit"] = 2;
        SassandraStats["Melee"] = 0;
        SassandraStats["Ranged"] = 4;
        SassandraStats["Fire"] = 8;
        SassandraStats["Ice"] = 8;
        SassandraStats["Buff"] = 0;
        SassandraStats["Defense"] = 10;

        LarsStats = new Dictionary<string, int>();
        LarsStats["MaxHP"] = 80;
        LarsStats["CurrentHP"] = 78;
        LarsStats["MaxSpirit"] = 2;
        LarsStats["Melee"] = 0;
        LarsStats["Ranged"] = 6;
        LarsStats["Fire"] = 0;
        LarsStats["Ice"] = 0;
        LarsStats["Buff"] = 10;
        LarsStats["Defense"] = 5;

        UgudStats = new Dictionary<string, int>();
        UgudStats["MaxHP"] = 140;
        UgudStats["CurrentHP"] = 120;
        UgudStats["MaxSpirit"] = 1;
        UgudStats["Melee"] = 20;
        UgudStats["Ranged"] = 1;
        UgudStats["Fire"] = 0;
        UgudStats["Ice"] = 0;
        UgudStats["Buff"] = 0;
        UgudStats["Defense"] = 20;




        PlayerAttacks = new Dictionary<string, List<string>>();

        PlayerAttacks["Main"] = new List<string>();
        PlayerAttacks["Main"].Add("Strike");
        PlayerAttacks["Main"].Add("Cleave");

        PlayerAttacks["Sassandra"] = new List<string>();
        PlayerAttacks["Sassandra"].Add("Dagger Throw");
        PlayerAttacks["Sassandra"].Add("Flechettes");

        PlayerSpells = new Dictionary<string, List<string>>();
        PlayerSpells["Sassandra"] = new List<string>();
        PlayerSpells["Sassandra"].Add("Frostbolt");
        PlayerSpells["Sassandra"].Add("Fireball");

        PlayerSpells["Main"] = new List<string>();
        PlayerSpells["Main"].Add("War Cry");

        Moves = new Dictionary<string, Move>();
        Moves["Strike"] = new Move("Strike", "Attack", "Melee", 0, 0, false, 0, 1, 1);
        Moves["Cleave"] = new Move("Cleave", "Attack", "Melee", 0, 0, true, 0, 1, 1);
        Moves["Dagger Throw"] = new Move("Cleave", "Attack", "Ranged", 0, 0, false, 0, 1, 1);
        Moves["Flechettes"] = new Move("Flechettes", "Attack", "Ranged", 0, 0, true, 0, 1, 1);
        Moves["Fireball"] = new Move("Fireball", "Spell", "Fire", 1, 0, false, 0, 1, 1);
        Moves["Frostbolt"] = new Move("FrostBolt", "Spell", "Ice", 0, 1, false, 0, 1, 1);
        Moves["War Cry"] = new Move("War Cry", "Spell", "Ranged", 1, 0, true, 1, 2, 1);
        Moves["Jellyfish Jelly"] = new Move("Jellyfish Jelly", "Item", "Ranged", 0, 0, false, 1, 30, 1);
        Moves["Vapor"] = new Move("Vapor", "Item", "Ranged", 0, 0, true, 1, 10, 1);
        //Get skill descriptions
        SkillDescriptions = new Dictionary<string, string>();
        StreamReader file = new StreamReader("skills.txt");
        string line;
        while ((line = file.ReadLine()) != null)
        {
            int index = line.IndexOf(":");
            if (index > -1)
            {
                int length = line.Length;
                SkillDescriptions[line.Substring(0, index)] = line.Substring(index + 2, length - (index + 2));

            }
        }

     



    }
}
public class Move {

    public string AttackName;
    public string Type; //Attack, Spell, Defend, Item
    public string DamageType; //Melee, Ranged, Fire, Ice
    public int Vulnerable;
    public int Weak;
    public bool IsAOE;
    public int Buff;
    public int Damage;
    public int SpiritCost;

        public Move(string AttackName, string Type, string DamageType, int Vulnerable, int Weak, bool AOE, int Buff, int Damage, int SpiritCost)
        {
            this.AttackName = AttackName;
            this.Type = Type;
            this.DamageType = DamageType;
            this.Vulnerable = Vulnerable;
            this.Weak = Weak;
            this.IsAOE = AOE;
            this.Buff = Buff;
            this.Damage = Damage;
            this.SpiritCost = SpiritCost;
        }

}
