using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class itemsMenu : MonoBehaviour {

	public AudioSource tick;
	public AudioSource cancelSound;
	public List<Text> quantities;
	public List<Text> selections;
	public Transform selectionBox;
	public GameObject upArrow, downArrow;
	public PauseMenuManager mainMenu;
	public Text description;
	public Image itemIcon;
	public GameObject memberChooser;
	public GameObject confirmer;
	private int x0 = -80;
	private int y0 = 180;
	public int dx, dy;

	private Color gray = new Color(.7f, .7f, .7f);
	private int itemSelected = 0;
	private int rowsOffset = 0;
	private bool wasUp = false;
	private bool wasDown = false;
	private bool wasRight = false;
	private bool wasLeft = false;
	private bool wasSelected = true;
	private bool wasEscaping = false;
	private MemberChooser chooser;
	private Confirmer confirmerScript;
	private List<string> singleConsumables
		= new List<string> {"Jellyfish Jelly"};

	private List<string> groupConsumables
		= new List<string> {"Vapor", "Skillixir"};

	bool isEquipment(string s) {
		return s.Contains("Lute");
	}

	bool isUsable(string s) {
		return singleConsumables.Contains(s) || groupConsumables.Contains(s);
	}

	void displaySelected() {
		//update offset first
		while(itemSelected < 2 * rowsOffset) {
			rowsOffset--;
		}

		while(itemSelected >= selections.Count + 2 * rowsOffset) {
			rowsOffset++;
		}

		//display
		int printed = -2 * rowsOffset;
		foreach(KeyValuePair<string, int> item in PlayerStats.items) {
			if(isUsable(item.Key)) {
				if(printed >= 0 && printed < selections.Count) {
					selections[printed].text = item.Key;
					selections[printed].color = Color.white;
					quantities[printed].text = item.Value.ToString();
					quantities[printed].color = Color.white;
				}
				printed++;
			}
		}

		foreach(KeyValuePair<string, int> item in PlayerStats.items) {
			if(isEquipment(item.Key)) {
				if(printed >= 0 && printed < selections.Count) {
					selections[printed].text = item.Key;
					selections[printed].color = Color.green;
					quantities[printed].text = item.Value.ToString();
					quantities[printed].color = Color.green;
				}
				printed++;
			}
		}

		foreach(KeyValuePair<string, int> item in PlayerStats.items) {
			if(!isUsable(item.Key) && !isEquipment(item.Key)) {
				if(printed >= 0 && printed < selections.Count) {
					selections[printed].text = item.Key;
					selections[printed].color = gray;
					quantities[printed].text = item.Value.ToString();
					quantities[printed].color = gray;
				}
				printed++;
			}
		}

		while(printed < selections.Count) {
			selections[printed].text = "";
			quantities[printed].text = "";
			printed++;
		}

		upArrow.SetActive(rowsOffset > 0);
		downArrow.SetActive(2 * rowsOffset + selections.Count < PlayerStats.items.Count);

		//update texts and images here
		string itemName = "";
		if(selections.Count > 0)
			itemName = selections[itemSelected - 2 * rowsOffset].text;

		description.text = ItemInfo.getDesc(itemName);
		itemIcon.sprite = ItemInfo.getIcon(itemName);
		if(itemIcon.sprite == null)
			print("can't find sprite");

		//move box to the proper location
		int row = itemSelected / 2 - rowsOffset;
		int col = itemSelected % 2;
		selectionBox.localPosition = new Vector2(x0 + dx * col, y0 + dy * row);
	}

	void Start() {
		chooser = memberChooser.GetComponent<MemberChooser>();
		confirmerScript = confirmer.GetComponent<Confirmer>();
	}

	void OnEnable() {
		itemSelected = 0;
		rowsOffset = 0;
		displaySelected();
	}

	void Update () {
		updateSelections();

		bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space);
		if(!wasSelected && isSelected) {
			userSelect();
		}
		wasSelected = isSelected;

		bool isEscaping = Input.GetKey(KeyCode.Escape);
		if(isEscaping && !wasEscaping) {
			cancelSound.Play();
			if(memberChooser.activeSelf) {
				memberChooser.SetActive(false);
			}
			else if (confirmer.activeSelf) {
				confirmer.SetActive(false);
			}
			else {
				mainMenu.enabled = true;
				gameObject.SetActive(false);
			}
		}
		wasEscaping = isEscaping;
	}

	void userSelect() {
		string itemName = selections[itemSelected - 2 * rowsOffset].text;
		if(memberChooser.activeSelf) {
			ItemInfo.useItem(itemName, chooser.getSelected());
			memberChooser.SetActive(false);
			if(itemSelected == PlayerStats.items.Count)
				itemSelected--;
			if(itemSelected < 0)
				itemSelected = 0;
			displaySelected();
		}
		else if (confirmer.activeSelf) {
			if(confirmerScript.isYes())
				equipItem(itemName);
			confirmer.SetActive(false);
		}
		else {
			if(singleConsumables.Contains(itemName)) {
				memberChooser.SetActive(true);
				return;
			}
			else if(groupConsumables.Contains(itemName)) {
				ItemInfo.useItem(itemName);
				if(itemSelected == PlayerStats.items.Count)
					itemSelected--;
				if(itemSelected < 0)
					itemSelected = 0;
				displaySelected();
				return;
			}
			else if (isEquipment(itemName)) {
				confirmer.SetActive(true);
				return;
			}
			else {
				print("using unusable item");
				return;
			}
		}
	}

	void equipItem(string itemName) {
		string equipper = "Lars"; //TODO
		string currentlyEquipped = PlayerStats.weaponsEquipped[equipper];
		if(currentlyEquipped != "----------") {
			PlayerStats.items[currentlyEquipped] = 1;
		}
		PlayerStats.weaponsEquipped[equipper] = itemName;
		PlayerStats.items.Remove(itemName);
		displaySelected();
	}

	void updateSelections() {

		bool isUp = Input.GetKey(KeyCode.UpArrow);
		bool isDown = Input.GetKey(KeyCode.DownArrow);
		bool isLeft = Input.GetKey(KeyCode.LeftArrow);
		bool isRight = Input.GetKey(KeyCode.RightArrow);
		bool changed = false;

		if(!wasUp && isUp) {
			if(memberChooser.activeSelf) {
				chooser.up();
			}
			else {
				if(itemSelected >= 2) {
					changed = true;
					itemSelected -= 2;
				}
			}
		}

		if(!wasDown && isDown) {
			if(memberChooser.activeSelf) {
				chooser.down();
			}
			else {
				if(itemSelected + 2 < PlayerStats.items.Count) {
					changed = true;
					itemSelected += 2;
				}
			}
		}

		if(!wasLeft && isLeft) {
			if(confirmer.activeSelf) {
				confirmerScript.toYes();
			}
			else if(itemSelected > 0 && !memberChooser.activeSelf) {
				changed = true;
				itemSelected--;
			}
		}

		if(!wasRight && isRight) {
			if(confirmer.activeSelf) {
				confirmerScript.toNo();
			}
			else if(itemSelected + 1 < PlayerStats.items.Count && !memberChooser.activeSelf) {
				changed = true;
				itemSelected++;
			}
		}

		wasUp = isUp;
		wasDown = isDown;
		wasRight = isRight;
		wasLeft = isLeft;

		if(changed) {
			tick.Play();
			displaySelected();
		}
	}
}
