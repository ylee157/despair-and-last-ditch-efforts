﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemberChooser : MonoBehaviour {

	public Transform arrow;
	private int index;

	void OnEnable() {
		index = 0;
		float ax = arrow.localPosition.x;
		arrow.localPosition = new Vector2(ax, 150 - 100 * index);
	}

	public void up() {
		index--;
		if(index < 0)
			index += PlayerStats.partyMembers.Count;
		float ax = arrow.localPosition.x;
		arrow.localPosition = new Vector2(ax, 150 - 100 * index);
	}

	public void down() {
		index++;
		if(index == PlayerStats.partyMembers.Count)
			index = 0;
		float ax = arrow.localPosition.x;
		arrow.localPosition = new Vector2(ax, 150 - 100 * index);
	}

	public string getSelected() {
		return PlayerStats.partyMembers[index];
	}
}
