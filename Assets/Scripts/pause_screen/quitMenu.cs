using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class quitMenu : MonoBehaviour {

	public Text yes;
	public Text no;

	public AudioSource tick;
	public AudioSource cancelSound;
	public PauseMenuManager mainMenu;
	private bool quitting = false;
	private Color gray = new Color(.7f, .7f, .7f);
	private bool wasMoving = false;
	private bool wasSelected = true;

	void highlightSelected() {
		if(quitting) {
			yes.color = Color.white;
			no.color = gray;
		}
		else {
			yes.color = gray;
			no.color = Color.white;
		}
	}

	void Start () {
		highlightSelected();
	}

	void Update () {
		updateSelections();

		bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space);
		if(!wasSelected && isSelected) {
			userSelect();
		}
		wasSelected = isSelected;
	}

	void userSelect() {
		if(quitting) {
			print("You think that'd work?");
		}
		else {
			cancelSound.Play();
			mainMenu.enabled = true;
			gameObject.SetActive(false);
		}

	}

	void updateSelections() {

		bool isMoving = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);

		if(!wasMoving && isMoving) {
			quitting = !quitting;
			highlightSelected();
			tick.Play();
		}

		wasMoving = isMoving;
	}
}
