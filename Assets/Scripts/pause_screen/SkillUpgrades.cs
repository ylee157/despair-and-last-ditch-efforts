using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class SkillUpgrades : MonoBehaviour {

	public List<Text> selections;
	public List<Text> boxes;
	public Text desc;
	public Text upgradesRemainingText;
	public PauseMenuManager mainMenu;
	public AudioSource tick;
	public AudioSource selectSound;
	public AudioSource cancelSound;
	public int maxLevel;
	private int itemSelected = 0;
	private Color gray = new Color(.5f, .5f, .5f);
	private bool wasUp = false;
	private bool wasDown = false;
	private bool wasHorizontal = false;
	private bool wasSelected = true;

	void highlightSelected() {
		foreach(Text t in selections)
			t.color = gray;
		selections[itemSelected].color = Color.white;
		desc.text = SkillInfo.getDesc(selections[itemSelected].text);
	}

	void updateUpgradeDisplays() {
		StringBuilder squares = new StringBuilder();
		for(int i = 0; i < PlayerStats.upgradesAvailable; ++i)
			squares.Append("■");
		upgradesRemainingText.text = squares.ToString();
		for(int i = 0; i < 6; ++i) {
			squares.Length = 0;
			for(int j = 0; j < PlayerStats.upgrades[selections[i].text]; ++j)
				squares.Append("■");
			for(int j = squares.Length; j < maxLevel; ++j)
				squares.Append("□");
			boxes[i].text = squares.ToString();
		}
	}

	void Update () {
		updateSelections();

		bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space);
		if(!wasSelected && isSelected) {
			userSelect();
		}
		wasSelected = isSelected;

		if(Input.GetKey(KeyCode.Escape)) {
			cancelSound.Play();
			mainMenu.enabled = true;
			gameObject.SetActive(false);
		}
	}

	void OnEnable() {
		itemSelected = 0;
		wasSelected = true;
		highlightSelected();
		updateUpgradeDisplays();
	}

	void userSelect() {
		if(PlayerStats.upgradesAvailable == 0) {
			print("No upgrades left");
			return;
		}
		string upgradeName = selections[itemSelected].text;
		if(PlayerStats.upgrades[upgradeName] == maxLevel) {
			print("already fully upgraded");
			return;
		}
		selectSound.Play();
		PlayerStats.upgrades[upgradeName]++;
		PlayerStats.upgradesAvailable--;
		updateUpgradeDisplays();
	}

	void updateSelections() {

		bool isUp = Input.GetKey(KeyCode.UpArrow);
		bool isDown = Input.GetKey(KeyCode.DownArrow);
		bool isHorizontal = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);
		bool changed = false;

		if(!wasUp && isUp) {
			changed = true;
			itemSelected -= 2;
			if(itemSelected < 0)
				itemSelected += selections.Count;
		}

		if(!wasDown && isDown) {
			changed = true;
			itemSelected += 2;
			if(itemSelected >= selections.Count)
				itemSelected -= selections.Count;
		}

		if(!wasHorizontal && isHorizontal) {
			changed = true;
			if(itemSelected % 2 != 0)
				itemSelected--;
			else
				itemSelected++;
		}

		wasUp = isUp;
		wasDown = isDown;
		wasHorizontal = isHorizontal;

		if(changed) {
			tick.Play();
			highlightSelected();
		}
	}
}
