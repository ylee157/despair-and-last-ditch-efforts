using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour {

		public List<Text> selections;
		public GameObject skillMenu;
		public GameObject characterMenu;
		public GameObject quitMenu;
		public GameObject itemMenu;
		public GameObject settingsMenu;

		public AudioSource tick;
		public AudioSource selectSound;
		private int itemSelected = 0;
		private Color gray = new Color(.7f, .7f, .7f);
		private bool wasUp = false;
		private bool wasDown = false;
		private bool wasSelected = false;

		void highlightSelected() {
			foreach(Text t in selections)
				t.color = gray;
			selections[itemSelected].color = Color.white;
		}

		// Use this for initialization
		void Start () {
			highlightSelected();
		}

		// Update is called once per frame
		void Update () {
			updateSelections();

			bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.RightArrow);
			if(!wasSelected && isSelected) {
				userSelect();
			}
			wasSelected = isSelected;
		}

		void userSelect() {
			selectSound.Play();
			switch(itemSelected) {
				case 0: {
					print("resume NYI");
					break;
				}
				case 1: {
					itemMenu.SetActive(true);
					enabled = false;
					break;
				}
				case 2: {
					skillMenu.SetActive(true);
					enabled = false;
					break;
				}
				case 3: {
					characterMenu.SetActive(true);
					enabled = false;
					break;
				}
				case 4: {
					settingsMenu.SetActive(true);
					enabled = false;
					break;
				}
				case 5: {
					quitMenu.SetActive(true);
					enabled = false;
					break;
				}
				default:
					throw new InvalidOperationException("invalid selection");
			}
		}

		void updateSelections() {

			bool isUp = Input.GetKey(KeyCode.UpArrow);
			bool isDown = Input.GetKey(KeyCode.DownArrow);

			bool changed = false;

			if(!wasUp && isUp) {
				changed = true;
				itemSelected--;
				if(itemSelected < 0)
					itemSelected += selections.Count;
			}

			if(!wasDown && isDown) {
				changed = true;
				itemSelected++;
				if(itemSelected == selections.Count)
					itemSelected = 0;
			}

			wasUp = isUp;
			wasDown = isDown;

			if(changed) {
				highlightSelected();
				tick.Play();
			}
		}
}
