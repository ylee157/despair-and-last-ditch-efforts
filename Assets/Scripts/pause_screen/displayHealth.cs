using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class displayHealth : MonoBehaviour {

	public Text name;
	private Text mText;
	private String memberName;

	void Awake() {
		mText = GetComponent<Text>();
	}

	void OnEnable() {
		refresh();
	}

	public void refresh() {
		Dictionary<string, Dictionary<string, int>> stats = new Dictionary<string, Dictionary<string, int>>();
		stats["MAIN"] = PlayerStats.MainStats;
		stats["Sassandra"] = PlayerStats.SassandraStats;
		stats["Lars"] = PlayerStats.LarsStats;
		stats["Ugud"] = PlayerStats.UgudStats;

		memberName = name.text;
		if(memberName == "???" || memberName == "You")
			memberName = "MAIN";
		mText.text = "HP: " + stats[memberName]["CurrentHP"].ToString() + "/" + stats[memberName]["MaxHP"].ToString();
	}
}
