using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Confirmer : MonoBehaviour {

	public Transform arrow;

	void OnEnable() {
		toNo();
	}

	public void toYes() {
		arrow.localPosition = new Vector2(-130, -35);
	}

	public void toNo() {
		arrow.localPosition = new Vector2(60, -35);
	}

	public bool isYes() {
		return arrow.localPosition.x == -130;
	}
}
