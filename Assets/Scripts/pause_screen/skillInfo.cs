﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo {

	private static Dictionary<string, string> desc;

	static SkillInfo() {
		desc = new Dictionary<string, string>();
		desc["Fire"] = "Affects Fire Bolts and Fireballs.";
		desc["Ice"] = "Affects Ray of Frost and Ice Storm.";
		desc["Melee"] = "Affects melee attacks.";
		desc["Ranged"] = "Affects arrows and explosives.";
		desc["Defense"] = "Affects defense.";
		desc["Buff"] = "Affects Haste, Stoneskin, and Heroism.";
	}

	public static string getDesc(string s) {
		if(desc.ContainsKey(s))
			return desc[s];
		return "Cannot get description for " + s ;
	}
}
