using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour {

	public AudioSource cancelSound;
	public AudioSource tick;
	public PauseMenuManager mainMenu;
	public Text name;
	public Text memberClass;
	public Text health;
	public Text weapon;
	public Text bio;

	private int index;
	private bool wasLeft;
	private bool wasRight;
	private int count;
	private Dictionary<string, string> charClasses;
	private Dictionary<string, string> bios;

	void Awake() {
		charClasses = new Dictionary<string, string>();
		charClasses["MAIN"] = "Human Fighter";
		charClasses["Sassandra"] = "Drow Sorceress";
		charClasses["Ugud"] = "Orc Barbarian";
		charClasses["Lars"] = "Human Bard";

		bios = new Dictionary<string, string>();
		bios["MAIN"] = "You!";
		bios["Sassandra"] = "A dark elf who has a way with magic and some serious anger problems.";
		bios["Ugud"] = "A fearsome orc, always accompanied by his trust battleaxe, Cleaver.";
		bios["Lars"] = "A wandering bard with a quick tongue and a sly wit.";
	}

	void OnEnable() {
		index = 0;
		wasLeft = true;
		wasRight = true;
		updateDisplays();
		count = PlayerStats.partyMembers.Count;
	}

	void updateDisplays() {
		name.text = PlayerStats.partyMembers[index];
		memberClass.text = charClasses[name.text];
		string memberName = name.text;
		if(memberName == "???")
			memberName = "MAIN";
		health.text = "HP: " + PlayerStats.currentHealth[memberName].ToString() + "/" + PlayerStats.maxHealth[memberName].ToString();

		weapon.text = PlayerStats.weaponsEquipped[memberName];

		bio.text = bios[memberName];
	}

	void Update () {
		if(Input.GetKey(KeyCode.Escape)) {
			cancelSound.Play();
			mainMenu.enabled = true;
			gameObject.SetActive(false);
		}

		bool isLeft = Input.GetKey(KeyCode.LeftArrow);
		bool isRight = Input.GetKey(KeyCode.RightArrow);

		int lastIndex = index;
		if(!wasLeft && isLeft) {
			index--;
			if(index < 0)
				index += count;
		}
		if(!wasRight && isRight) {
			index++;
			if(index == count)
				index = 0;
		}

		if(index != lastIndex){
			updateDisplays();
			tick.Play();
		}

		wasLeft = isLeft;
		wasRight = isRight;
	}
}
