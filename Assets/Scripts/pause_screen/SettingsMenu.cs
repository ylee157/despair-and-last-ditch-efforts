﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

	public PauseMenuManager mainMenu;
	public AudioSource cancelSound;
	public AudioSource tick;
	public AudioSource selectSound;
	// public AudioListener audioListener;

	public Text muteBox;
	public List<Text> selections;
	private Color gray = new Color(.7f, .7f, .7f);
	private int itemSelected;
	private bool wasUp;
	private bool wasDown;
	private bool wasSelected;

	void highlightSelected() {
		foreach(Text t in selections)
			t.color = gray;
		selections[itemSelected].color = Color.white;
	}

	void OnEnable () {
		if(Settings.muted)
			muteBox.text = "■";
		else
			muteBox.text = "□";
		itemSelected = 0;
		wasUp = true;
		wasDown = true;
		wasSelected = true;
		highlightSelected();
		AudioListener.volume = Settings.muted ? 0f : 1f;
	}

	void Update () {
		if(Input.GetKey(KeyCode.Escape)) {
			cancelSound.Play();
			mainMenu.enabled = true;
			gameObject.SetActive(false);
		}

		bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.RightArrow);
		if(!wasSelected && isSelected) {
			userSelect();
		}
		wasSelected = isSelected;

		bool isDown = Input.GetKey(KeyCode.DownArrow);
		bool isUp = Input.GetKey(KeyCode.UpArrow);

		bool changed = false;

		if(!wasUp && isUp) {
			changed = true;
			itemSelected--;
			if(itemSelected < 0)
				itemSelected += selections.Count;
		}

		if(!wasDown && isDown) {
			changed = true;
			itemSelected++;
			if(itemSelected == selections.Count)
				itemSelected = 0;
		}

		wasUp = isUp;
		wasDown = isDown;

		if(changed) {
			highlightSelected();
			tick.Play();
		}
	}

	void userSelect() {
		selectSound.Play();
		switch(itemSelected) {
			case 0: {
				Settings.muted = !Settings.muted;
				muteBox.text = Settings.muted ? "■" : "□";
				AudioListener.volume = Settings.muted ? 0f : 1f;
				break;
			}
			case 1: {
				print("PH brightness");
				break;
			}
			case 2: {
				mainMenu.enabled = true;
				gameObject.SetActive(false);
				break;
			}
		}
	}

}
