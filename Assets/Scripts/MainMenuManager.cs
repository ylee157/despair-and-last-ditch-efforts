using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

	public List<Text> mainSelections;
	public List<Text> settingsSelections;

	private List<Text> currentSelections;

	public GameObject mainMenu;
	public GameObject settingsMenu;

	public AudioSource cancelSound;
	public AudioSource selectSound;
	public AudioSource tick;

	private int itemSelected = 0;
	private Color gray = new Color(.7f, .7f, .7f);

	private bool wasUp = false;
	private bool wasDown = false;

	private bool wasSelected = false;
	private bool inSetting = false;

	// Use this for initialization
	void Start () {

		currentSelections = mainSelections;
		foreach(Text t in currentSelections)
			t.color = gray;
		currentSelections[itemSelected].color = Color.white;
	}

	// Update is called once per frame
	void Update () {
		updateSelections();

		bool isSelected = Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space);
		if(!wasSelected && isSelected) {
			userSelect();
		}
		wasSelected = isSelected;
	}

	void userSelect() {
		if(!inSetting) {
			switch(itemSelected) {
				case 0: {
					selectSound.Play();
					print("continue NYI");
					break;
				}
				case 1: {
					selectSound.Play();
                        SceneManager.LoadScene("Level1");
                        print("new game NYI");
					break;
				}
				case 2: {
					selectSound.Play();

					itemSelected = 0;
					mainMenu.SetActive(false);
					settingsMenu.SetActive(true);
					currentSelections = settingsSelections;
					inSetting = true;

					foreach(Text t in currentSelections)
						t.color = gray;
					currentSelections[itemSelected].color = Color.white;

					break;
				}
				case 3: {
					print("quit NYI");
					break;
				}
				default:
					throw new InvalidOperationException("invalid selection");
			}
		}
		else {
			switch(itemSelected) {
				case 0: {
					selectSound.Play();
					print("PH1 NYI");
					break;
				}
				case 1: {
					selectSound.Play();
					print("PH2 NYI");
					break;
				}
				case 2: {
					cancelSound.Play();
					itemSelected = 0;
					mainMenu.SetActive(true);
					settingsMenu.SetActive(false);
					currentSelections = mainSelections;
					inSetting = false;

					foreach(Text t in currentSelections)
						t.color = gray;
					currentSelections[itemSelected].color = Color.white;

					break;
				}
				default:
					throw new InvalidOperationException("invalid selection");
			}
		}
	}

	void updateSelections() {

		bool isUp = Input.GetKey(KeyCode.UpArrow);
		bool isDown = Input.GetKey(KeyCode.DownArrow);

		bool changed = false;

		if(!wasUp && isUp) {
			changed = true;
			itemSelected--;
			if(itemSelected < 0)
				itemSelected += currentSelections.Count;
		}

		if(!wasDown && isDown) {
			changed = true;
			itemSelected++;
			if(itemSelected == currentSelections.Count)
				itemSelected = 0;
		}

		wasUp = isUp;
		wasDown = isDown;

		if(changed) {
			tick.Play();
			foreach(Text t in currentSelections) {
				t.color = gray;
			}
			currentSelections[itemSelected].color = Color.white;
		}
	}
}
