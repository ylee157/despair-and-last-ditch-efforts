﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Camera : MonoBehaviour {
    private GameObject player;
    public static string prevScene = "";
    public static string currScene = "";
    public static bool canPause = true;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        if(currScene != "")
        {
            prevScene = currScene;
            //currScene = SceneManager.GetActiveScene().name;
        }
	}
	
	// Update is called once per frame
	void LateUpdate () {
        currScene = SceneManager.GetActiveScene().name;
        if (player != null)
        {
            gameObject.transform.position = new Vector3(Mathf.Clamp(player.transform.position.x, minX, maxX),
                Mathf.Clamp(player.transform.position.y + 1.5f, minY, maxY), gameObject.transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currScene == "Battle" || !canPause)
            {
                return;
            } else if (currScene != "pause")
            {
                Debug.Log(currScene);
                SceneManager.LoadScene("pause");
            }
            else
            {
                Debug.Log(currScene);
                SceneManager.LoadScene(prevScene);
            }
        }
        
    }
}
