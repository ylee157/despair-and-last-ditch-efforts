﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class HeroBase  {

    public string name;
    public int hp;
    public int maxhp;
    public int Melee;
    public int Ranged;
    public int Fire;
    public int Ice;
    public int Buff;
    public int Block;
    public int BlockAmount;
    public int CurrentSpirit;
    public int MaxSpirit;
    public List<string> Attacks;
    public List<string> Spells;
    public string HeroName;
    public int VulnerableTurns;
    public int WeakTurns;
    public int Strength;
}
