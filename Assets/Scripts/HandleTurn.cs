﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class HandleTurn {
    public string AttackerName;
    public GameObject Attacker;
    public GameObject Target;
    public string Type;
    public string Move; //Attack, Defend, Spell
    public string AttackName;
}
