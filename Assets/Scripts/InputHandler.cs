﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour {

    // Use this for initialization
    public List<Button> Player1Buttons;
    public List<Button> Player2Buttons; //hey guys remember to switch all these to private later
    public List<Button> CurrentPlayerButtons;

    private List<string> ValidItems;
    private List<string> CurrentItems;

    public int CurrentPos;
    public int CurrentEnemyPos;
    private int CurrentItem;

    public SelectState CurrentState;
    public string CurrentMove;
    public string CurrentMoveName;

    private Color gray = new Color(.7f, .7f, .7f);

    private BattleStateMachine BSM;
    HeroStateMachine CurrentPlayer;
    HeroStateMachine Player1;
    HeroStateMachine Player2;

    public GameObject Player1Panel;
    public GameObject Player2Panel;

    private GameObject ItemPanel;
    private Image ItemImage;
    private GameObject none_left;

    private List<string> CurrentAttacks;
    private List<string> CurrentSpells;
    private int CurrentAttackPos;
    private int CurrentSpellPos;
    private GameObject AttackText;
    private GameObject AttackPanel;

    private AudioSource menu_sound;
    private AudioSource menu_enter;
    private AudioSource item_sfx;

    private Text move_description;
    private Text SpiritCostText;

    public enum SelectState
    {
        MENU,
        ENEMYSELECT,
        HEROSELECT,
        MOVESELECT,
        WAITING,
        ITEMSELECT,
        SELECTATTACK,
        SELECTALLENEMIES,
        SPELLSELECT
    }

    void Start () {
        BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
        Player1 = GameObject.Find("Hero").GetComponent<HeroStateMachine>();
        Player2 = GameObject.Find("Hero_2").GetComponent<HeroStateMachine>();

        Player1Panel = GameObject.Find("menu_panel");
        Player2Panel = GameObject.Find("menu_panel_2");

        Player1Buttons.Add(GameObject.Find("attack_button").GetComponent<Button>());
        Player1Buttons.Add(GameObject.Find("defend_button").GetComponent<Button>());
        Player1Buttons.Add(GameObject.Find("spell_button").GetComponent<Button>());
        Player1Buttons.Add(GameObject.Find("item_button").GetComponent<Button>());

        Player2Buttons.Add(GameObject.Find("attack_button_2").GetComponent<Button>());
        Player2Buttons.Add(GameObject.Find("defend_button_2").GetComponent<Button>());
        Player2Buttons.Add(GameObject.Find("spell_button_2").GetComponent<Button>());
        Player2Buttons.Add(GameObject.Find("item_button_2").GetComponent<Button>());

        menu_sound = GameObject.Find("menu_sound").GetComponent<AudioSource>();
        menu_enter = GameObject.Find("menu_enter").GetComponent<AudioSource>(); ;
        item_sfx = GameObject.Find("item_sfx").GetComponent<AudioSource>(); ;

        CurrentPlayerButtons = Player1Buttons;
        CurrentPlayer = Player1;
        CurrentEnemyPos = 0;
        CurrentPos = 0;
        CurrentState = SelectState.MENU;
        highlight(gray);

        Player1Panel.SetActive(true);
        Player2Panel.SetActive(false);

        none_left = GameObject.Find("none_left");
        none_left.SetActive(false);

        ItemPanel = GameObject.Find("item_panel");
        ItemImage = GameObject.Find("item_pic").GetComponent<Image>();
        ItemPanel.SetActive(false);

        ValidItems = new List<string>();
        ValidItems.Add("Vapor");
        ValidItems.Add("Jellyfish Jelly");

        CurrentItems = new List<string>();


        foreach (KeyValuePair<string, int> entry in PlayerStats.items)
        {
            if (ValidItems.Contains(entry.Key))
            {
                CurrentItems.Add(entry.Key);
            }
        }

        move_description = GameObject.Find("move_description").GetComponent<Text>();
        move_description.text = "";
    }

    // Update is called once per frame
    void Update () {
        if (BSM.EnemiesInBattle.Count < 0) { return; }
        switch (CurrentState)
        {
            case SelectState.MENU:
                SelectMenu();
                break;
            case SelectState.ENEMYSELECT:
                if (CurrentEnemyPos < 0)
                {
                    CurrentEnemyPos++;
                }
                else if (CurrentEnemyPos > BSM.EnemiesInBattle.Count - 1)
                {
                    CurrentEnemyPos--;
                }
                FlashEnemy(CurrentEnemyPos);
                SelectEnemy();
                break;
            case SelectState.WAITING:
                break;
            case SelectState.ITEMSELECT:
                SelectItem();
                break;
            case SelectState.SELECTATTACK:
                SelectAttack();
                break;
            case SelectState.SELECTALLENEMIES:
                SelectAllEnemies();
                break;
            default:break;
        }
    }

    private void SelectItem()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            menu_sound.Play();
            CurrentItem = mod(CurrentItem + 1, CurrentItems.Count);
            UpdateItemImage();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            menu_sound.Play();
            CurrentItem = mod(CurrentItem - 1, CurrentItems.Count);
            UpdateItemImage();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            if (PlayerStats.items[CurrentItems[CurrentItem]] > 0) {
                item_sfx.Play();
                if (CurrentPlayer.move("Item", CurrentItems[CurrentItem], BSM.EnemiesInBattle[CurrentEnemyPos]))
                {

                    ItemPanel.SetActive(false);
                    if (CurrentPlayer.hero.CurrentSpirit == 0)
                    {
                        SwitchPlayer();
                    }
                    CurrentState = SelectState.MENU;
                }


            } else
            {
                menu_enter.Play();
                none_left.SetActive(true);
                Invoke("DisableText", 1f);
            }
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            menu_enter.Play();
            ItemPanel.SetActive(false);
            CurrentState = SelectState.MENU;
        }

    }
    private void UpdateItemImage()
    {
        ItemImage.sprite = ItemInfo.getIcon(CurrentItems[CurrentItem]);

    }
    private void SelectEnemy()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            menu_sound.Play();
            SetEnemyVisible(CurrentEnemyPos);
            CurrentEnemyPos = mod(CurrentEnemyPos + 1, BSM.EnemiesInBattle.Count);
        } else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            menu_sound.Play();
            SetEnemyVisible(CurrentEnemyPos);
            CurrentEnemyPos = mod(CurrentEnemyPos - 1, BSM.EnemiesInBattle.Count);
        } else if (Input.GetKeyDown(KeyCode.Z))
        {
            SetEnemyVisible(CurrentEnemyPos);
            menu_enter.Play();
            if (CurrentPlayer.move(CurrentMove, CurrentAttacks[CurrentAttackPos], BSM.EnemiesInBattle[CurrentEnemyPos]))
            {
                AttackPanel.SetActive(false);
                move_description.text = "";
                if (CurrentPlayer.hero.CurrentSpirit == 0)
                {
                    SwitchPlayer();
                }
                CurrentState = SelectState.MENU;
            }
        } else if (Input.GetKeyDown(KeyCode.X))
        {
            menu_enter.Play();
            SetEnemyVisible(CurrentEnemyPos);
            CurrentState = SelectState.SELECTATTACK;
        }
    }
    private void FlashEnemy(int position)
    {
        SpriteRenderer s = BSM.EnemiesInBattle[position].GetComponent<SpriteRenderer>();
        s.color = Color.red;

    }

    private void SetEnemyVisible(int position)
    {
        SpriteRenderer s = BSM.EnemiesInBattle[position].GetComponent<SpriteRenderer>();
        s.color = Color.white;

    }

    private void SelectMenu()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            menu_sound.Play();
            highlight(Color.white);
            CurrentPos = mod(CurrentPos + 1, 4);
            highlight(gray);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            menu_sound.Play();
            highlight(Color.white);
            CurrentPos = mod(CurrentPos - 1, 4);
            highlight(gray);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            menu_enter.Play();
            SwitchPlayer();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            menu_enter.Play();
            switch (CurrentPos)
            {
                case 0:
                    CurrentMove = "Attack";
                    AttackPanel = CurrentPlayer.AttackPanel;
                    AttackPanel.SetActive(true);
                    CurrentAttackPos = 0;
                    CurrentAttacks = CurrentPlayer.hero.Attacks;
                    CurrentState = SelectState.SELECTATTACK;
                    AttackText = GameObject.Find(CurrentPlayer.AttackName);
                    SpiritCostText = GameObject.Find(CurrentPlayer.spirit_cost_name).GetComponent<Text>();
                    UpdateAttack();
                    break;
                case 1:
                    CurrentMove = "Defend";
                    if (CurrentPlayer.move("Defend", "Defend", BSM.EnemiesInBattle[CurrentEnemyPos]) && CurrentPlayer.hero.CurrentSpirit == 0)
                    {
                        SwitchPlayer();
                    }

                    break;
                case 2:
                    CurrentMove = "Spell";
                    AttackPanel = CurrentPlayer.AttackPanel;
                    AttackPanel.SetActive(true);
                    CurrentAttackPos = 0;
                    CurrentAttacks= CurrentPlayer.hero.Spells;
                    CurrentState = SelectState.SELECTATTACK;
                    AttackText = GameObject.Find(CurrentPlayer.AttackName);
                    SpiritCostText = GameObject.Find(CurrentPlayer.spirit_cost_name).GetComponent<Text>();
                    UpdateAttack();
                    break;

                case 3:
                    /*if (CurrentPlayer == Player1)
                    {
                        Player1Panel.SetActive(false);
                    }
                    else if (CurrentPlayer == Player2)
                    {
                        Player2Panel.SetActive(false);
                    }*/

                    ItemPanel.SetActive(true);
                    CurrentState = SelectState.ITEMSELECT;
                    UpdateItemImage();
                    break;
                default:
                    break;
            }
        }  else if (Input.GetKeyDown(KeyCode.Return))
        {
            menu_enter.Play();
            CurrentPlayer.hero.CurrentSpirit = 0;
            SwitchPlayer();
        }
    }


    private void UpdateAttack()
    {
        Text t = AttackText.GetComponent<Text>();
        t.text = CurrentAttacks[CurrentAttackPos];
        //  CurrentMove = CurrentAttacks[CurrentAttackPos];
        SpiritCostText.text = PlayerStats.Moves[CurrentAttacks[CurrentAttackPos]].SpiritCost + "";
        move_description.text = PlayerStats.SkillDescriptions[CurrentAttacks[CurrentAttackPos]];
    }

    private void highlight(Color c)
    {
        Button b = CurrentPlayerButtons[CurrentPos];
        ColorBlock cb = b.colors;
        cb.normalColor = c;
        b.colors = cb;
    }

    private int mod(int x, int m)
    {
        return (x % m + m) % m;
    }

    private void SwitchPlayer()
    {
        highlight(Color.white);
        if (CurrentPlayerButtons == Player1Buttons && Player2.hero.CurrentSpirit != 0)
        {
            CurrentPlayerButtons = Player2Buttons;
            CurrentPlayer = Player2;
            Player1Panel.SetActive(false);
            Player2Panel.SetActive(true);

            CurrentPos = 0;
            highlight(gray);
        }
        else if (CurrentPlayerButtons == Player2Buttons && Player1.hero.CurrentSpirit != 0)
        {
            CurrentPlayerButtons = Player1Buttons;
            CurrentPlayer = Player1;
            Player2Panel.SetActive(false);
            Player1Panel.SetActive(true);

            CurrentPos = 0;
            highlight(gray);
        }
    }


    void SelectAttack()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            menu_sound.Play();
            CurrentAttackPos = mod(CurrentAttackPos + 1, CurrentAttacks.Count);
            UpdateAttack();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            menu_sound.Play();
            CurrentAttackPos = mod(CurrentAttackPos - 1, CurrentAttacks.Count);
            UpdateAttack();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {

            Debug.Log(CurrentAttacks[CurrentAttackPos]);
            if (!PlayerStats.Moves[CurrentAttacks[CurrentAttackPos]].IsAOE)
            {
                CurrentState = SelectState.ENEMYSELECT;
            } else
            {
                for (int i = 0; i < BSM.EnemiesInBattle.Count; i++)
                {
                    FlashEnemy(i);
                }
                CurrentState = SelectState.SELECTALLENEMIES;

            }
            /* else
            {
                CurrentMove = "Attack";
                //TODO change CurrentAttacks[CurrentAttackPos] to CurrentAttackName
                if (CurrentPlayer.move(CurrentMove, CurrentAttacks[CurrentAttackPos], BSM.EnemiesInBattle[CurrentEnemyPos]))
                {
                    AttackPanel.SetActive(false);
                    if (CurrentPlayer.hero.CurrentSpirit == 0)
                    {
                        SwitchPlayer();
                    }
                    CurrentState = SelectState.MENU;
                }
            }*/
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            menu_enter.Play();
            AttackPanel.SetActive(false);
            CurrentState = SelectState.MENU;
            move_description.text = "";
        }

    }

    void DisableText()
    {
        none_left.SetActive(false);
    }

    void SelectAllEnemies()
    {

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (CurrentPlayer.move(CurrentMove, CurrentAttacks[CurrentAttackPos], BSM.EnemiesInBattle[CurrentEnemyPos]))
            {
                for (int i = 0; i < BSM.EnemiesInBattle.Count; i++)
                {
                    SetEnemyVisible(i);
                }
                AttackPanel.SetActive(false);
                move_description.text = " ";
                if (CurrentPlayer.hero.CurrentSpirit == 0)
                {
                    SwitchPlayer();
                }
                CurrentState = SelectState.MENU;

            }
        } else if (Input.GetKeyDown(KeyCode.X))
        {
            for (int i = 0; i < BSM.EnemiesInBattle.Count; i++)
            {
                SetEnemyVisible(i);
            }
            menu_enter.Play();
            CurrentState = SelectState.SELECTATTACK;
        }
    }
}
