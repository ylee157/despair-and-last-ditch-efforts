using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class ItemInfo {

	private static Dictionary<string, string> desc;
	private static Dictionary<string, Sprite> icons;
	private static Sprite placeholderSprite;
	private static Sprite blankSprite;

	static ItemInfo() {
		System.IO.StreamReader file = new System.IO.StreamReader(@"Assets/scripts/items.txt");
		Regex delim = new Regex(" *= *");
		string line;

		desc = new Dictionary<string, string>();

		while((line = file.ReadLine()) != null) {
			string[] substrings = delim.Split(line);
			desc[substrings[0]] = substrings[1];
		}
/*
		desc["dead graveworm"] = "A vicious otherworldly monster. Was killed by a scythe.";
		desc["jellyfish jelly"] = "Restores 50 health. Don't ask.";
		desc["Hue's paintball gun"] = "Coloring your world since 2017!";
		desc["wooden lute"] = "Beautifully crafted; Essential for bards.";
		desc["red lute"] = "Increases the effectiness of Haste.";
		desc["white lute"] = "Increase the effectiveness of Mending.";
		desc["black lute"] = "It's electric. Replaces Song of Ouch with Song of Death.";
		desc["healthpack"] = "Restores 10 health to all party members.";
		desc["elixir of skill"] = "Grants 1 skill upgrade.";
		desc["branch"] = "a random branch off the floor";
		desc["garbage"] = "Why are we carrying this again?";
*/		desc[""] = "";

		icons = new Dictionary<string, Sprite>();
		placeholderSprite = Resources.Load<Sprite>("Art/pause_screen/item_icons/placeholder");
		blankSprite = Resources.Load<Sprite>("Art/pause_screen/item_icons/blank");
		icons[""] = blankSprite;
	}

	public static string getDesc(string s) {
		if(desc.ContainsKey(s))
			return desc[s];
		return "Cannot get description for " + s ;
	}

	public static Sprite getIcon(string itemName) {
		if(!icons.ContainsKey(itemName)) {
			Sprite s = Resources.Load<Sprite>("Art/pause_screen/item_icons/" + itemName.Replace(" ", "_").Replace("'", ""));
			if(s == null)
				s = placeholderSprite;
			icons[itemName] = s;
		}

		return icons[itemName];
	}

	public static void useItem(string itemName, string target = "") {
		Dictionary<string, Dictionary<string, int>> stats = new Dictionary<string, Dictionary<string, int>>();
		stats["MAIN"] = PlayerStats.MainStats;
		stats["Sassandra"] = PlayerStats.SassandraStats;
		stats["Lars"] = PlayerStats.LarsStats;
		stats["Ugud"] = PlayerStats.UgudStats;

		switch(itemName) {
			case "Skillixir":
				PlayerStats.upgradesAvailable++;
				break;
			case "Vapor":
				foreach(string member in PlayerStats.partyMembers) {
					stats[member]["CurrentHP"] =
							Math.Min(stats[member]["CurrentHP"] + 10,
									stats[member]["MaxHP"]);
				}
				break;
			case "Jellyfish Jelly":
				stats[target]["CurrentHP"] = Math.Min(stats[target]["CurrentHP"] + 50,
						stats[target]["MaxHP"]);
				break;
			default:
				break;
		}

		if(PlayerStats.items[itemName] == 1)
			PlayerStats.items.Remove(itemName);
		else
			PlayerStats.items[itemName]--;

	}

}
