﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class EnemyStats
{
    public static Dictionary<string, List<string>> Battles; //From Battle ID to List of Enemies in the battle
    public static Dictionary<string, Dictionary<string, int>> EnemyBaseStats; // From Enemy type to a dictionary of their base stats
    public static Dictionary<string, List<string>> EnemyAttacks; // From enemy type to their list of attacks.
    public static Dictionary<string, int> Exp;
    public static string CurrentBattle;

    static EnemyStats()
    {
        Battles = new Dictionary<string, List<string>>();
        EnemyBaseStats = new Dictionary<string, Dictionary<string, int>>();
        EnemyAttacks = new Dictionary<string, List<string>>();
        Exp = new Dictionary<string, int>();

        //remove later because its attached to prefab
        EnemyBaseStats["Jellyfish"] = new Dictionary<string, int>();
        EnemyBaseStats["Jellyfish"]["hp"] = 50;
        EnemyBaseStats["Jellyfish"]["attack"] = 10;
        EnemyBaseStats["Jellyfish"]["defense"] = 10;
        EnemyBaseStats["Jellyfish"]["index"] = 0;   //that's pretty wild

        EnemyBaseStats["Crab"] = new Dictionary<string, int>(); 
        EnemyBaseStats["Crab"]["index"] = 1;   //that's pretty wild

        //actually implement this so the enemy uses its specific attacks
        EnemyAttacks["Jellyfish"] = new List<string>
        {
            "AOEAttack",
            "Defend",
            "Strength_up"
        };

        EnemyAttacks["Crab"] = new List<string>
        {
            "Attack",
            "Defend",
            "Weaken"
        };

        Battles["twojellyfish"] = new List<string>();
        Battles["twojellyfish"].Add("Jellyfish");
        Battles["twojellyfish"].Add("Jellyfish");
        Exp["twojellyfish"] = 40;

        Battles["onejellyfish"] = new List<string>();
        Battles["onejellyfish"].Add("Jellyfish");
        Exp["onejellyfish"] = 20;

        Battles["twocrabs"] = new List<string>();
        Battles["twocrabs"].Add("Crab");
        Battles["twocrabs"].Add("Crab");
        Exp["twocrabs"] = 50;

        Battles["twocrabs"] = new List<string>();
        Battles["twocrabs"].Add("Crab");
        Battles["twocrabs"].Add("Crab");
        Exp["twocrabs"] = 50;

        Battles["jellyfishcrab"] = new List<string>();
        Battles["jellyfishcrab"].Add("Jellyfish");
        Battles["jellyfishcrab"].Add("Crab");
        Exp["jellyfishcrab"] = 45;

        CurrentBattle = "twocrabs";
    }

}
