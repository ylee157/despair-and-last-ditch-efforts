﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleStateMachine : MonoBehaviour {

    public enum PerformAction
    {
        WAIT,
        TAKEACTION,
        PERFORMACTION,
        PLAYERACTION,
        END,
        LEVELUP
    }

    public List<GameObject> prefabs;

    HeroBase h1;
    HeroBase h2;
    public PerformAction battleStates;
    public List<HandleTurn> HeroPerformList = new List<HandleTurn>();
    public List<HandleTurn> EnemyPerformList = new List<HandleTurn>();

    public List<GameObject> HeroesInBattle = new List<GameObject>();
    public List<GameObject> EnemiesInBattle = new List<GameObject>();

    InputHandler h;
    private int SpiritCounter;
    private GameObject level_up_panel;

    // public List<GameObject> HeroPanels = new List<GameObject>();
    // Use this for initialization
    void Start () {
        List<string> Enemies = EnemyStats.Battles[EnemyStats.CurrentBattle];
        level_up_panel = GameObject.Find("level_up");
        level_up_panel.SetActive(false);

        int i = 0;
        foreach (string s in Enemies)
        {
            GameObject o = Instantiate(prefabs[EnemyStats.EnemyBaseStats[s]["index"]]);

            if (i == 0)
            {
                o.GetComponent<Transform>().position = new Vector3(3, -1.5f, 90);
                o.name = "Enemy";
            } else
            {
                o.GetComponent<Transform>().position = new Vector3(5, -1.5f, 90);
                EnemyStateMachine e = o.GetComponent<EnemyStateMachine>();
                o.name = "Enemy_2";
                e.enemy.name = "Enemy_2";
                e.imageTag = "choice_2";
                e.panel_name = "enemy_panel_2";
                e.hp_text_name = "enemy_hp_2";
                e.block_text_name = "enemy_block_2";
                e.block_anim_name = "enemy_block_anim_2";
                e.strength_name += "_2";
                e.vulnerable_name += "_2";
                e.weak_name += "_2";
                e.buff_loc = new Vector3(42.5f, -40, 0);
                e.enemy.EnemyName = s;
            }

            i++;
        }

        if (Enemies.Count < 2)
        {
            GameObject.Find("enemy_panel_2").SetActive(false);
            GameObject.Find("enemy_block_anim_2").SetActive(false);
        }


        battleStates = PerformAction.WAIT;
        EnemiesInBattle.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
        HeroesInBattle.AddRange(GameObject.FindGameObjectsWithTag("hero"));
        //HeroPanels.AddRange(GameObject.FindGameObjectsWithTag("hero_panel"));
        h = GameObject.Find("InputHandler").GetComponent<InputHandler>();

        h1 = GameObject.Find("Hero").GetComponent<HeroStateMachine>().hero;
        h2 = GameObject.Find("Hero_2").GetComponent<HeroStateMachine>().hero;

        h1.HeroName = "Main";
        h1.hp = PlayerStats.MainStats["CurrentHP"];
        h1.maxhp = PlayerStats.MainStats["MaxHP"];
        h1.Melee = PlayerStats.MainStats["Melee"] + 2 * PlayerStats.upgrades["Melee"];
        h1.BlockAmount = PlayerStats.MainStats["Defense"] + PlayerStats.upgrades["Defense"];
        h1.Ranged = PlayerStats.MainStats["Ranged"] + 2 * PlayerStats.upgrades["Ranged"];
        h1.Fire = PlayerStats.MainStats["Fire"] + PlayerStats.upgrades["Fire"];
        h1.Ice = PlayerStats.MainStats["Ice"] + PlayerStats.upgrades["Ice"];
        h1.Buff = PlayerStats.MainStats["Buff"] + PlayerStats.upgrades["Buff"];
        h1.BlockAmount = PlayerStats.MainStats["Defense"] + PlayerStats.upgrades["Defense"];
        h1.MaxSpirit = PlayerStats.MainStats["MaxSpirit"];
        h1.CurrentSpirit = h1.MaxSpirit;
        h1.Attacks = PlayerStats.PlayerAttacks["Main"];
        h1.Spells = PlayerStats.PlayerSpells["Main"];

        h2.HeroName = "Sassandra";
        h2.hp = PlayerStats.SassandraStats["CurrentHP"];
        h2.maxhp = PlayerStats.SassandraStats["MaxHP"];
        h2.Melee = PlayerStats.SassandraStats["Melee"] + 2 * PlayerStats.upgrades["Melee"];
        h2.BlockAmount = PlayerStats.SassandraStats["Defense"] + PlayerStats.upgrades["Defense"];
        h2.Ranged = PlayerStats.SassandraStats["Ranged"] + 2 * PlayerStats.upgrades["Ranged"];
        h2.Fire = PlayerStats.SassandraStats["Fire"] + PlayerStats.upgrades["Fire"];
        h2.Ice = PlayerStats.SassandraStats["Ice"] + PlayerStats.upgrades["Ice"];
        h2.Buff = PlayerStats.SassandraStats["Buff"] + PlayerStats.upgrades["Buff"];
        h2.BlockAmount = PlayerStats.SassandraStats["Defense"] + PlayerStats.upgrades["Defense"];
        h2.MaxSpirit = PlayerStats.SassandraStats["MaxSpirit"];
        h2.CurrentSpirit = h2.MaxSpirit;
        h2.Attacks = PlayerStats.PlayerAttacks["Sassandra"];
        h2.Spells = PlayerStats.PlayerSpells["Sassandra"];

    }

    // Update is called once per frame
    void Update () {

        if (battleStates != PerformAction.END && battleStates != PerformAction.LEVELUP && EnemiesInBattle.Count == 0)
        {
            PlayerStats.Experience += EnemyStats.Exp[EnemyStats.CurrentBattle];

            if (PlayerStats.Experience >= 100)
            {
                PlayerStats.Experience = PlayerStats.Experience - 100;
                PlayerStats.upgradesAvailable += 2;
                battleStates = PerformAction.LEVELUP;
                level_up_panel.SetActive(true);
                h.Player1Panel.SetActive(false);
                h.Player2Panel.SetActive(false);
                h.CurrentState = InputHandler.SelectState.WAITING;
                Debug.Log("level up");
            }
            else
            {
                Debug.Log("BATTLE ENDING");
                battleStates = PerformAction.END;
            }


        }

        switch (battleStates)
        {
            case PerformAction.WAIT:

                CountSpirit();

                if (HeroPerformList.Count > 0)
                {
                    battleStates = PerformAction.PLAYERACTION;
                    StopPlayerInput();

                } else if (SpiritCounter == 0 && EnemyPerformList.Count == EnemiesInBattle.Count)
                {
                    battleStates = PerformAction.TAKEACTION;
                }
                break;

            case PerformAction.PLAYERACTION:

                TakeAction(HeroPerformList[0]);

                break;

            case PerformAction.TAKEACTION:
                foreach (GameObject o in EnemiesInBattle)
                {
                    o.GetComponent<EnemyStateMachine>().choice_image.gameObject.SetActive(false);
                }

                if (EnemyPerformList.Count > 0)
                {
                    TakeAction(EnemyPerformList[0]);
                } else {
                    battleStates = PerformAction.WAIT;

                    //Sets players back to selecting moves
                    foreach (GameObject o in HeroesInBattle)
                    {
                        HeroStateMachine b = o.GetComponent<HeroStateMachine>();
                        b.currentState = HeroStateMachine.TurnState.SELECTING;

                        b.hero.CurrentSpirit = b.hero.MaxSpirit;
                    }

                    //Sets enemies back to selecting moves
                    foreach (GameObject o in EnemiesInBattle)
                    {
                        EnemyBase b = o.GetComponent<EnemyStateMachine>().enemy;
                        o.GetComponent<EnemyStateMachine>().currentState = EnemyStateMachine.TurnState.CHOOSEACTION;
                        battleStates = PerformAction.WAIT;
                        o.GetComponent<EnemyStateMachine>().choice_image.gameObject.SetActive(true);
                        if (b.WeakTurns > 0)
                        {
                            b.WeakTurns--;
                        }

                        if (b.VulnerableTurns > 0)
                        {
                            b.VulnerableTurns--;
                        }
                        
                    }

                    //Allows input handler to start taking player input again
                    h.CurrentState = InputHandler.SelectState.MENU;

                    //Display player panels
                    if (h.CurrentPlayerButtons == h.Player1Buttons)
                    {
                        h.Player1Panel.SetActive(true);
                    }
                    else
                    {
                        h.Player2Panel.SetActive(true);
                    }
                    
                    
                }
               
                break;
            case PerformAction.PERFORMACTION:
                CountSpirit();
                StopPlayerInput();
                break;

            case PerformAction.END:
                PlayerStats.MainStats["CurrentHP"] = h1.hp;
                PlayerStats.SassandraStats["CurrentHP"] = h2.hp;
                SceneManager.LoadScene("Level1");
                break;
            case PerformAction.LEVELUP:

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    level_up_panel.SetActive(false);
                    battleStates = PerformAction.END;
                }
                break;
            default:break; 
        }	
	}

    private void CountSpirit()
    {
        SpiritCounter = 0;
        foreach (GameObject o in HeroesInBattle)
        {
            SpiritCounter += o.GetComponent<HeroStateMachine>().hero.CurrentSpirit;
        }
    }

    private void StopPlayerInput()
    {
        if (SpiritCounter == 0)
        {
            h.CurrentState = InputHandler.SelectState.WAITING;
            h.Player1Panel.SetActive(false);
            h.Player2Panel.SetActive(false);
        }
    }
    public void TakeAction(HandleTurn temp)
    {
        
        GameObject performer = GameObject.Find(temp.AttackerName);

        if (temp.Type == "Enemy")
        {
            if (performer == null)
            {
                EnemyPerformList.RemoveAt(0);
                return;
            } else
            {
                EnemyStateMachine ESM = performer.GetComponent<EnemyStateMachine>();
                ESM.currentTurn = temp;
                ESM.currentState = EnemyStateMachine.TurnState.ACTION;
               
            }
        }

        if (temp.Type == "Hero")
        {
            if (performer == null)
            {
                HeroPerformList.RemoveAt(0);
                return;
            }
            else
            {
                HeroStateMachine HSM = performer.GetComponent<HeroStateMachine>();
                HSM.currentTurn = temp;
                HSM.currentState = HeroStateMachine.TurnState.ACTION;
            }
        }

        battleStates = PerformAction.PERFORMACTION;
    }

    public void CollectActions(HandleTurn input)
    {
        if (input.Type == "Enemy")
        {
            EnemyPerformList.Add(input);
        } else if (input.Type == "Hero")
        {
            HeroPerformList.Add(input);
        } else
        {
            Debug.Log("Invalid HandleTurn type being passed into BSM");
        }
    }

}
