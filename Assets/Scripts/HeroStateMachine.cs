﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HeroStateMachine : MonoBehaviour {

    public HeroBase hero;
    private BattleStateMachine BSM;
    //public GameObject EnemyToAttack;
    public HandleTurn currentTurn;
    //public string panelTag;
    //private GameObject panel;
    public string attack_button;
    public string defend_button;
    private Text hp_text;
    private Text block_text;
    private Text spirit_text;
    public string hp_text_name;
    public string block_text_name;
    public string spirit_text_name;
    private GameObject no_spirit;
    public string no_spirit_name;
    public string block_anim_name;
    public string spirit_cost_name;
    private SpriteRenderer block_anim;
    public string AttackName;
    //private Text AttackText;
    public string AttackPanelName;
    public GameObject AttackPanel;
    public float temp;


    public string strength_name;
    public string vulnerable_name;
    public string weak_name;

    private Text strength_text;
    private Text vulnerable_text;
    private Text weak_text;

    public enum TurnState
    {
        SELECTING,
        ACTION,
        DEAD,
        WAITING
    }

    public TurnState currentState;
    private bool actionStarted = false;

	// Use this for initialization
	void Start () {
        BSM = GameObject.Find("BattleManager").GetComponent<BattleStateMachine>();
        //panel = GameObject.Find(panelTag);
        no_spirit = GameObject.Find(no_spirit_name);
        hp_text = GameObject.Find(hp_text_name).GetComponent<Text>();
        block_text = GameObject.Find(block_text_name).GetComponent<Text>();
        spirit_text = GameObject.Find(spirit_text_name).GetComponent<Text>();
        block_anim = GameObject.Find(block_anim_name).GetComponent<SpriteRenderer>();
        AttackPanel = GameObject.Find(AttackPanelName);
        AttackPanel.SetActive(false);
        currentState = TurnState.SELECTING;
        no_spirit.SetActive(false);

        strength_text = GameObject.Find(strength_name).GetComponent<Text>();
        vulnerable_text = GameObject.Find(vulnerable_name).GetComponent<Text>();
        weak_text = GameObject.Find(weak_name).GetComponent<Text>();

        SetAlpha(block_anim, 0);

    }

	// Update is called once per frame
	void Update () {

        //If we get slowdown, move this
        hp_text.text = hero.hp + "/" + hero.maxhp;
        block_text.text = hero.Block + "";
        spirit_text.text = hero.CurrentSpirit + "";
        vulnerable_text.text = hero.VulnerableTurns + "";
        weak_text.text = hero.WeakTurns + "";
        strength_text.text = hero.Strength + "";

        switch (currentState)
        {
            case TurnState.SELECTING:
                /*if (!panel.activeSelf)
                {
                    panel.SetActive(true);
                }
  */              break;
            case TurnState.ACTION:
                StartCoroutine(TimeForAction());
                break;
            case TurnState.DEAD:
                break;
            case TurnState.WAITING:
                break;
            default: break;
        }
	}

    public bool move(string moveType, string moveName, GameObject Target)
    {
        Debug.Log(moveType);
        Move m;
        if (moveName == "Defend")
        {
            m = null;
        } else 
        {
            m = PlayerStats.Moves[moveName];
        }
        if (!CanMove(m))
        {
            no_spirit.SetActive(true);
            Invoke("DisableText", 1f);
            return false;
        }


        HandleTurn myAttack = new HandleTurn();
        myAttack.AttackName = moveName;
        myAttack.Move = moveType;
        myAttack.Target = Target;

        /*
        switch (moveType)
        {
            case "Attack":
                myAttack.AttackName = moveType;
                myAttack.Move = "Attack";
                myAttack.Target = Target; //will this create errors?
                break;

            case "Defend":
                myAttack.Move = "Defend";
                break;

            case "Item":
                myAttack.Move = "Item";
                myAttack.Target = this.gameObject;
                break;

        }*/
        myAttack.AttackerName = hero.name;
        myAttack.Type = "Hero";
        myAttack.Attacker = this.gameObject;
        BSM.CollectActions(myAttack);
    //    panel.SetActive(false);
        currentState = TurnState.WAITING;

        return true;
    }


    //TODO: Delete this and replace with a better method to keep track of moves and stuff later
    private bool CanMove(Move m)
    {
        int SpiritCost;

        if (m == null) {
            SpiritCost = 2;
        } else {
            SpiritCost = m.SpiritCost;
        }

        if (SpiritCost > hero.CurrentSpirit)
        {
            Debug.Log(SpiritCost);
            return false;
        } else
        {
            hero.CurrentSpirit -= SpiritCost;
            return true;
        }
    }
    private IEnumerator TimeForAction()
    {
        if (actionStarted)
        {
            yield break;
        }

        actionStarted = true;

        Animator a = this.gameObject.GetComponent<Animator>();
        if (currentTurn.Move == "Attack")
        {
            a.Play("attack");
            this.gameObject.GetComponent<AudioSource>().Play();
        }
        else if (currentTurn.Move == "Spell")
        {
            a.Play("attack");
            GameObject.Find("spell_sfx").GetComponent<AudioSource>().Play();
        }
        else if (currentTurn.Move == "Defend")
        {
            StartCoroutine(Fadein(block_anim));
            GameObject.Find("block_animations").GetComponent<AudioSource>().Play();
        }

        yield return new WaitForSeconds(1f);
        HandleMove();

        BSM.HeroPerformList.RemoveAt(0);

        actionStarted = false;

        currentState = TurnState.WAITING;
        //BSM.battleStates = BattleStateMachine.PerformAction.TAKEACTION;
        BSM.battleStates = BattleStateMachine.PerformAction.WAIT;

        //Basically, we're gonna use this to animate the enemy when they attack.
    }

    private void HandleMove()
    {

        EnemyBase target = currentTurn.Target.GetComponent<EnemyStateMachine>().enemy; // what the fuck is wrong with this line
        if (currentTurn.Move == "Defend")
        {

            hero.Block += hero.BlockAmount;
        }
        else if (currentTurn.Move == "Attack")
        {
            Move m = PlayerStats.Moves[currentTurn.AttackName];
            if (!m.IsAOE)
            {
                currentTurn.Target.GetComponent<Animator>().Play("hurt");
                AttackEnemy(currentTurn.Target, m);
            } else
            {/*
                foreach (GameObject o in BSM.EnemiesInBattle)
                {
                    AttackEnemy(o, m);
                }*/

                List<GameObject> temp = new List<GameObject>();
                foreach (GameObject o in BSM.EnemiesInBattle)
                {
                    temp.Add(o);
                }

                for (int i = 0; i < temp.Count; i++)
                {
                    temp[i].GetComponent<Animator>().Play("hurt");
                    AttackEnemy(temp[i], m);
                }
            }

            //GONNA NEED THIS WHEN ATTACKS HAVE DIFFERENT EFFECTS
            switch (currentTurn.AttackName)
            {
                default:break;
            }

        }
        else if (currentTurn.Move == "Spell")
        {
            Move m = PlayerStats.Moves[currentTurn.AttackName];

            //If not a buff, then process the attack
            if (m.Buff < 1)
            {
                if (!m.IsAOE)
                {
                    currentTurn.Target.GetComponent<Animator>().Play("hurt");
                    AttackEnemy(currentTurn.Target, m);
                }
                else
                {
                    int Count = BSM.EnemiesInBattle.Count;
                    for (int i = 0; i < Count; i++)
                    {
                        BSM.EnemiesInBattle[i].GetComponent<Animator>().Play("hurt");
                        AttackEnemy(BSM.EnemiesInBattle[i], m);
                    }
                }
            }

            switch (currentTurn.AttackName)
            {
                case "Fireball":
                    target.VulnerableTurns += m.Vulnerable;
                    break;
                case "Frostbolt":
                    target.WeakTurns += m.Weak;
                    break;
                case "War Cry":
                    foreach (GameObject o in BSM.HeroesInBattle)
                    {
                        o.GetComponent<HeroStateMachine>().hero.Strength += m.Damage;
                    }
                    foreach (GameObject o in BSM.EnemiesInBattle)
                    {
                        o.GetComponent<EnemyStateMachine>().enemy.VulnerableTurns += m.Vulnerable;
                    }
                    break;
            }
        } else if (currentTurn.Move == "Item")
        {
            useBattleItem(currentTurn.AttackName);
        }
    }

    private void AttackEnemy(GameObject o, Move m)
    {
        EnemyBase b = o.GetComponent<EnemyStateMachine>().enemy;

        int AttackDamage;

        switch (m.DamageType)
        {
            case "Melee":
                AttackDamage = hero.Melee + hero.Strength;
                break;
            case "Ranged":
                AttackDamage = hero.Ranged + hero.Strength;
                break;
            case "Ice":
                AttackDamage = hero.Ice;
                break;
            case "Fire":
                AttackDamage = hero.Fire;
                break;
            default:
                AttackDamage = hero.Melee;
                break;
        }

        if (m.IsAOE)
        {
            AttackDamage /= 2;
        }

        if (b.VulnerableTurns > 0)
        {
            AttackDamage = (int) (AttackDamage * 1.5);
        }

        if (hero.WeakTurns > 0)
        {
            AttackDamage = (int)(AttackDamage - (AttackDamage * 0.25));
        }



        if (b.Block > 0)
        {
            b.Block -= AttackDamage;
            if (b.Block < 0)
            {
                int extraDamage = b.Block;
                b.Block = 0;
                b.hp += extraDamage;
            }
        }
        else
        {
            b.hp -= (int)(AttackDamage);
        }
        if (b.hp <= 0)
        {
            b.hp = 0;
            o.GetComponent<EnemyStateMachine>().panel.SetActive(false);
            o.GetComponent<EnemyStateMachine>().currentState = EnemyStateMachine.TurnState.DEAD;
            BSM.EnemiesInBattle.Remove(o);
            for (int i = 0; i < BSM.EnemyPerformList.Count; i++)
            {
                if (BSM.EnemyPerformList[i].Attacker == o)
                {
                    BSM.EnemyPerformList.RemoveAt(i);
                }
            }
            o.SetActive(false);
        }
    }
    private IEnumerator Fadein(SpriteRenderer s)
    {
        for (float f = 0; f < 1; f += .1f)
        {
            SetAlpha(s, f);
            yield return null;
        }
        yield return new WaitForSeconds(.5f);
        SetAlpha(s, 0);
    }

    private void SetAlpha (SpriteRenderer s, float val)
    {
        Color ca = s.color;
        ca.a = val;
        s.color = ca;
    }

    void DisableText()
    {
        no_spirit.SetActive(false);
    }

    public void useBattleItem(string itemName/*, string target = ""*/)
    {

        //Item
        Move m = PlayerStats.Moves[currentTurn.AttackName];

        if (m.Buff > 0)
        {
            if (!m.IsAOE)
            {
                hero.hp = Math.Min(hero.hp + m.Damage, hero.maxhp);
            }
            else
            {
                foreach (GameObject o in BSM.HeroesInBattle)
                {
                    HeroBase curr = o.GetComponent<HeroStateMachine>().hero;
                    curr.hp = Math.Min(curr.hp + m.Damage , curr.maxhp);
                }
            }
        } else
        {
            //For when items are attacks and NOT buffs?
        }

        //Other specific item effects
        switch (itemName)
        {
            case "Vapor":
                break;
            case "Jellyfish Jelly":
            default:
                break;
        }

        PlayerStats.items[itemName]--;
    }
}
