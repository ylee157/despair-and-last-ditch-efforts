﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialoguePoint : MonoBehaviour {
    public string words;
    public bool heavyDialogue;
    public bool destroyOnExit = false;
    public string[] options;
    public string[] responses;
    public int[] indices;
    public List<int> dialogueEnders;
}
