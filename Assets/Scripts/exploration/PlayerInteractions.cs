﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerInteractions : MonoBehaviour {
    private int speedMult = 10;
    private float xSpeed;
    private float downwardV = -20.0f;
    public float jForce = 700.0f;
    private bool jumped = true;
    private bool jumping = true;
    private bool lSlope;
    private bool rSlope;
    public bool facingRight = true;
    private bool jumpButton;
    public bool frozen = false;
    private bool hittingWall = false;
    private int hpVal;
    private int maxHP;
    private int spiritVal;
    public int keysInt = 0;
    public Text hpText;
    public Text spiritText;
    public Text keysText;
    public Vector3 nextLevelPos;
    public string currentScene = "Level1";
    public string nextScene = "Level2";
    public static List<string> deadEnemies = new List<string>();
    public static List<string> invalidDialogue = new List<string>();
    public Dialogue dialogueManager;

    // Use this for initialization
    void Start () {
        foreach(string en in deadEnemies)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
            foreach( GameObject e in enemies)
            {
                if(e.gameObject.GetComponent<EnemyID>().ID == en)
                {
                    Destroy(e.gameObject);
                }
            }
        }
        foreach (string npc in invalidDialogue)
        {
            GameObject[] emptyDialogue = GameObject.FindGameObjectsWithTag("NPC");
            foreach (GameObject d in emptyDialogue)
            {
                if (d.gameObject.GetComponent<EnemyID>() != null && d.gameObject.GetComponent<EnemyID>().ID == npc)
                {
                    Destroy(d.gameObject);
                }
            }
        }
        spiritVal = PlayerStats.maxSpirit["MAIN"];
        maxHP = PlayerStats.maxHealth["MAIN"];
        hpVal = PlayerStats.currentHealth["MAIN"];
        transform.position = PlayerStats.playerPos;
        frozen = false;
    }

	// Update is called once per frame
	void FixedUpdate () {
        xSpeed = Input.GetAxis("Horizontal");
      //  jumpButton = Input.GetButtonDown("Jump");
        maxHP = PlayerStats.MainStats["MaxHP"];
        hpVal = PlayerStats.MainStats["CurrentHP"];
        spiritVal = PlayerStats.MainStats["MaxSpirit"];
        hpText.text = hpVal.ToString() + "/" + maxHP;
        spiritText.text = spiritVal.ToString();
        keysText.text = keysInt.ToString();

        if (gameObject.transform.position.y < -10)
        {
            SceneManager.LoadScene(currentScene);
        }
        PlayerStats.playerPos = transform.position;

    }

    private void OnTriggerExit2D(Collider2D trig)
    {
        if (trig.gameObject.tag == "NPC")
        {
            DialoguePoint npc = trig.gameObject.GetComponent<DialoguePoint>();
            if(npc.destroyOnExit == true)
            {
                invalidDialogue.Add(trig.gameObject.GetComponent<EnemyID>().ID);
                //Changing the destroy to SetActive(false) for a sec below
                trig.gameObject.SetActive(false);
                dialogueManager.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D trig)
    {
        //TODO Rewrite as switch case

        //if interacting with an NPC with a choice you are frozen
        if (trig.gameObject.tag == "NPC")
        {
            DialoguePoint npc = trig.gameObject.GetComponent<DialoguePoint>();
            //TODO What about non-heavy dialogues?
            dialogueManager.gameObject.SetActive(true);
            dialogueManager.talkToNPC(npc);
            if (npc.heavyDialogue)
            {
                frozen = true;
            }
        }

        //add dead enemies to a list and transitions to battle
        else if (trig.gameObject.tag == "enemy")
        {
            EnemyStats.CurrentBattle = trig.gameObject.name;
            deadEnemies.Add(trig.gameObject.GetComponent<EnemyID>().ID);
            SceneManager.LoadScene("Battle");
        }
        else if(trig.gameObject.tag == "Door" && keysInt > 0)
        {
            keysInt--;
            Destroy(trig.gameObject);
        }
        else if (trig.gameObject.tag == "key")
        {
            keysInt++;
            Destroy(trig.gameObject);
        }
        else if (trig.gameObject.tag == "goal")
        {
            PlayerStats.playerPos = nextLevelPos;
            SceneManager.LoadScene(nextScene);
        }
    }
}
