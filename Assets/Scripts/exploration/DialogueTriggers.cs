﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTriggers : MonoBehaviour {

    public static void decisionImpact(string choice)
    {

        if(choice == "[You gained Spirit!]")
        {
            PlayerStats.MainStats["MaxSpirit"]++;
        }
        else if(choice == "[You lost Spirit.]")
        {
            PlayerStats.MainStats["MaxSpirit"]--;
        }
    }

}
