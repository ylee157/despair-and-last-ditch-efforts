﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newFollower : MonoBehaviour {

	public float delay;
	public float dist;
	public Transform leader;
	private LinkedList<Vector3> records;
	private bool idle = true;
	private Collider2D collider;
	private ContactFilter2D filter;
	private Collider2D[] ret = new Collider2D[1];

	void Start () {
		records = new LinkedList<Vector3>();
		collider = GetComponent<Collider2D>();
		filter = new ContactFilter2D();
		filter.NoFilter();
		filter.useTriggers = false;
		idle = false;
	}

	bool isGrounded() {
		return collider.OverlapCollider(filter, ret) > 0;
	}

	void FixedUpdate () {

        if (records.Count == 0 || leader.position != records.Last.Value)
			records.AddLast(leader.position);
		if(idle) {
			if(records.Count > delay / Time.deltaTime)
				idle = false;
			if(!isGrounded())
				idle = false;
		}
		else {
            //flip sprite right
            if (transform.position.x < records.First.Value.x)
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
            //flip sprite left
            if (transform.position.x > records.First.Value.x)
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            transform.position = records.First.Value;
            records.RemoveFirst();
			if(isGrounded() && records.Count < dist / Time.deltaTime)
				idle = true;
		}
	}
}
