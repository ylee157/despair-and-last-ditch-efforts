﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float velocity; //5?
    public float jumpStrength; //factor of velocity (recommended 2)
	private float height;
	private float width;
	private float vy = 0;
	public float gAccel;
	private BoxCollider2D collider;
	private int jumpsAvailable;
	Collider2D[] nearbyColliders = new Collider2D[20];
	private int numColliders;
	private ContactFilter2D noFilter = (new ContactFilter2D()).NoFilter();
	private bool wasUp;
	private SpriteRenderer renderer;
    [SerializeField]
    private Collider2D capsuleCol;
    private int jumpingUp = 0;
	private Vector2 rightColliderPos;
    private Vector2 leftColliderPos;

	void Start () {
		collider = GetComponent<BoxCollider2D>();
		width = collider.size.x * transform.localScale.x;
		height = collider.size.y * transform.localScale.y;
        collider.size *= 1.5f;
		jumpsAvailable = 0;
		renderer = GetComponent<SpriteRenderer>();
		//gAccel = 4 * velocity;
		wasUp = true;
		rightColliderPos = collider.offset;
        leftColliderPos = new Vector2(collider.offset.x * -1, collider.offset.y);
	}

	void FixedUpdate () {
		numColliders = collider.OverlapCollider(noFilter, nearbyColliders);
		bool right = Input.GetKey(KeyCode.RightArrow);
		bool left = Input.GetKey(KeyCode.LeftArrow);
        if (!gameObject.GetComponent<PlayerInteractions>().frozen)
        {
            if (right && !left)
                horizontal(true);
            if (left && !right)
                horizontal(false);
            if (!left && !right)
                GetComponent<Animator>().SetBool("isIdle", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("isIdle", true);
        }
		vertical();
	}

	bool isValid(Vector2 pos) {
		for(int i = 0; i < numColliders; ++i) {
			Collider2D c = nearbyColliders[i];
            if (!c.isTrigger && c.OverlapPoint(pos) && c != capsuleCol)
            {
                return false;
            }
		}
		return true;
	}

	void horizontal(bool isRight) {
		//renderer and collider stuff

		renderer.flipX = !isRight;
		collider.offset = isRight ? rightColliderPos : leftColliderPos;

		//other stuff
		float dx = velocity * Time.deltaTime;
        GetComponent<Animator>().SetBool("isIdle", false);
		if(!isRight)
			dx = -dx;
		if(!isGrounded()) {
			//in air
			if(isValid(transform.position + new Vector3(dx, -height/2, 0)) &&
				isValid(transform.position + new Vector3(dx, -height/4, 0)) &&
				isValid(transform.position + new Vector3(dx, 0, 0)) &&
				isValid(transform.position + new Vector3(dx, height/4, 0)))
				transform.position += new Vector3(dx, 0, 0);
			return;
		} else {
			Vector2 feet = transform.position + new Vector3(dx, -height / 2, 0);
			Vector2 halfHeight = new Vector2(0, height/2);

			float increment = Mathf.Tan(Mathf.Deg2Rad * .3f);
			Vector2 dy = new Vector2(0, Time.deltaTime * velocity * increment);

			if(isValid(feet)) {
				//going downhill
				for(int i = 1; i * increment < 1; ++i) {
					if(!isValid(feet - i * dy)) {
						transform.position = feet - (i - 1) * dy + halfHeight;
						return;
					}
				}
				//just walked off a cliff
				transform.position = feet + halfHeight;
				jumpsAvailable = 1;
			} else {
				//climbing hill
				for(int i = 1; i * increment < 1; ++i) {
					feet += dy;
					if(isValid(feet)) {
						transform.position = feet + halfHeight;
						return;
					}
				}
			}
		}
	}

	public bool isGrounded() {
		return jumpsAvailable == 2;
	}

    void vertical() {
        bool upKey = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Space);
        bool up = !wasUp && upKey;
        wasUp = upKey;
        if(jumpingUp == 2)
        {
            GetComponent<Animator>().SetInteger("jumpingUp", 0);
        }
        if (isGrounded() && !up) //Do nothing if not in air
        {
            GetComponent<Animator>().SetInteger("jumpingUp", jumpingUp = 0);
            return;
        }

        if (up && jumpsAvailable > 0) {
            if (!gameObject.GetComponent<PlayerInteractions>().frozen)
            {
                GetComponent<Animator>().SetBool("landing", false);
                jumpsAvailable--;
                jumpingUp++;
                GetComponent<Animator>().SetInteger("jumpingUp", jumpingUp);
                vy = jumpStrength * velocity;
            }
            return;
        }

        if (up && jumpsAvailable == 0) {
            print("no more jumps left");
        }

        if (isGrounded())
        {
            GetComponent<Animator>().SetInteger("jumpingUp", jumpingUp = 0);
            return;
        }

		//if in air
		float dy = vy * Time.deltaTime;
		vy -= gAccel * Time.deltaTime;
		vy = Mathf.Clamp(vy, -3 * velocity, 3 * velocity);
		Vector2 newPos = transform.position + new Vector3(0, dy, 0);
		if(vy < 0) {
			Vector2 feet = newPos - new Vector2(0, height / 2);
			if(!isValid(feet)) {
				vy = 0;
				jumpsAvailable = 2;

				//smooth the landing
				float x = transform.position.x;
				float low = feet.y;
				float high = transform.position.y - height / 2;
				float guess = (low + high) / 2;
				for(int i = 0; i < 10; ++i) { //10 iteration of binary search
					if(isValid(new Vector2(x, guess)))
						high = guess;
					else
						low = guess;
					guess = (high + low) / 2;
				}

				//actual landing
				transform.position = new Vector2(x, high + height / 2);
                GetComponent<Animator>().SetBool("landing", true);
				return;
			}
		}
		transform.position = newPos;
	}
}
