﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyBase  {

    public string name;
    public int hp;
    public int maxhp;
    public int attackDamage;
    public int Block;
    public int BlockAmount;
    public string EnemyName;
    public int VulnerableTurns;
    public int WeakTurns;
    public int Strength;
}
