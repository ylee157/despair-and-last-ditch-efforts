﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMotion : MonoBehaviour {

    public Vector2[] destinations;
    public Vector2 currentTarget;
    private int targetIndex = 0;
    public float xVel;
    public float yVel;
	// Use this for initialization
	void Start () {
        currentTarget = destinations[targetIndex];
	}
	
	// Update is called once per frame
	void Update () {

        move();
        if (GetComponent<Collider2D>().OverlapPoint(currentTarget))
        {
            try
            {
                targetIndex++;
                currentTarget = destinations[targetIndex];
            }
            catch
            {
                targetIndex = 0;
                currentTarget = destinations[targetIndex];
            }
        }
    }
    private void move()
    {
        if (currentTarget.x - transform.position.x < 0)
        {
            transform.position -= new Vector3(xVel * Time.deltaTime, 0, 0);
        }
        else if (currentTarget.x - transform.position.x > 0)
        {
            transform.position += new Vector3(xVel * Time.deltaTime, 0, 0);
        }
        if (currentTarget.y - transform.position.y > 0)
        {
            transform.position += new Vector3(0, yVel * Time.deltaTime, 0);
        }
        else if (currentTarget.y - transform.position.y < 0)
        {
            transform.position -= new Vector3(0, yVel * Time.deltaTime, 0);
        }
    }
}
